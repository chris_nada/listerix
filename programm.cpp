#include "programm.hpp"
#include <fstream>
#include <FL/fl_ask.H>
#include <iostream>
#include <algorithm>

Programm::Programm() {
    fenster    = new Fl_Window(640, 420, "Listenhilfe");
    box_text   = new Fl_Box(40,40,560,100,
            "DE: Klicke den Favoriten.\n\n"
            "UA: Натисніть на тебе улюблений.");
    box_todo   = new Fl_Box(40,200,560,30, "");
    btn_links  = new Fl_Button(20,300,290,100," - ");
    btn_links->labelsize(24);
    btn_links->callback(auswahl, (void *)this);
    btn_rechts = new Fl_Button(320,300,300,100," - ");
    btn_rechts->labelsize(24);
    btn_rechts->callback(auswahl, (void *)this);
}

int Programm::start() {
    fenster->end();
    fenster->show();
    if (!liste_lesen()) exit(0);
    set_links(liste.at(0));
    set_rechts(liste.at(1));
    return Fl::run();
}

bool Programm::liste_lesen() {
    std::ifstream in("liste.txt");
    if (in.good()) {
        for (std::string zeile; std::getline(in, zeile);) {
            if (zeile.size() > 1) {
                Name name;
                name.name = zeile;
                liste.push_back(name);
            }
        }
        #ifndef NDEBUG
            for (const auto& p : liste) std::cout << p.name << '\n';
        #endif
        if (liste.size() < 5) {
            fl_alert("DE: Liste liste.txt zu kurz.\n"
                     "UA: liste.txt короткий.");
            return false;
        }
        return true;
    }
    fl_alert("DE: Datei liste.txt nicht gefunden.\n"
             "UA: liste.txt не існує.");
    return false;
}

void Programm::auswahl(Fl_Widget* w, void* data) {
    Programm* app = (Programm*) data;
    if (!(w && app)) return;

    std::string gewinner;
    std::string verlierer;
    if (w == app->btn_links) {
        gewinner = app->btn_links->label();
        verlierer   = app->btn_rechts->label();
    }
    if (w == app->btn_rechts) {
        gewinner = app->btn_rechts->label();
        verlierer   = app->btn_links->label();
    }
    #ifndef NDEBUG
        std::cout << "Favorit = " << gewinner << std::endl;
    #endif
    if (!gewinner.empty()) {
        size_t i_gewinner  = app->pos(gewinner);
        size_t i_verlierer = app->pos(verlierer);
        size_t i_liste = app->liste.size();
        if (i_gewinner < i_liste && i_verlierer < i_liste) { // gefunden

            // Score
            auto& pg = app->liste[i_gewinner];
            auto& pv = app->liste[i_verlierer];

            if (pg.score < pv.score) pg.score = pv.score + 1;
            else pg.score++;

            if (!pg.besser_als.count(pv.name) && !pv.besser_als.count(pg.name)) {
                pg.besser_als[pv.name] = true;
                pv.besser_als[pg.name] = false;
            }

            // Next
            app->next();
            return;
        }
        fl_alert("DE: Fehler bei der Auswertung:\n"
                 "UA: Помилка при оцінці. Будь ласка, допови:\n\n"
                 "i_gewinner || i_verlierer == app->liste.size()");
    }
    fl_alert("DE: Fehler bei der Auswertung:\n"
             "UA: Помилка при оцінці. Будь ласка, допови:\n\n"
             "gewinner.empty()");
    exit(0);
}

void Programm::set_links(Name& name) {
    slinks = name.name;
    btn_links->label(slinks.c_str());
    name.gesehen++;
}

void Programm::set_rechts(Name& name) {
    srechts = name.name;
    btn_rechts->label(srechts.c_str());
    name.gesehen++;
}

size_t Programm::pos(const std::string& eintrag) const {
    for (size_t i = 0; i < liste.size(); ++i) {
        if (liste[i].name == eintrag) return i;
    }
    return liste.size();
}

void Programm::next() {



    std::sort(liste.begin(), liste.end(), [](const Name& lhs, const Name& rhs) {
        if (lhs.gesehen != rhs.gesehen) return lhs.gesehen < rhs.gesehen;
        if (lhs.score   != rhs.score)   return lhs.score   < rhs.score;
        return lhs.name < rhs.name;
    });
    #ifndef NDEBUG
        for (const auto& n : liste) { std::cout << n.gesehen << ' '; } std::cout << '\n';
    #endif

    // Fertig ?
    auto n_gleich = anzahl_gleich();
    if (n_gleich == 0) {
        fl_alert("DE: Fertig. Sortierte Liste wird gespeichert als liste_x.txt.\n"
                 "UA: Готовo. Відсортований список зберігається як liste_x.txt.");

        std::sort(liste.begin(), liste.end(), [](const Name& lhs, const Name& rhs) { return lhs.score > rhs.score; });
        std::ofstream out("liste_x.txt");
        if (out.good()) {
            for (const auto& n : liste) out << n.name << '\n';
        }
        out << std::flush;
        exit(0);
    }

    // Abschätzung
    auto top = std::max_element(liste.begin(), liste.end(), [](const Name& lhs, const Name& rhs) {
        return lhs.score < rhs.score;
    });
    n_gleich = (n_gleich + liste.size()) / (1 + top->score);
    stodo = std::string("Noch etwa / Iще приблизно : " + std::to_string(n_gleich));
    box_todo->label(stodo.c_str());

    // Score gleich nehmen
    repeat:
    for (auto& name1 : liste) {
        for (auto& name2 : liste) {
            if (name1.name != name2.name && name1.score == name2.score) {

                // Bereits verglichen?
                if (name1.besser_als.count(name2.name)) {
                    if (name1.besser_als[name2.name]) name1.score++;
                    else name2.score++;
                    goto repeat;
                }

                set_links(name1);
                set_rechts(name2);
                return;
            }
        }
    }

    // Am wenigsten gesehene nehmen
    set_links(liste[0]);
    set_rechts(liste[1]);
}

size_t Programm::anzahl_gleich() {
    size_t gleiche = 0;
    for (const auto& name1 : liste) {
        for (const auto& name2 : liste) {
            if (name1.name != name2.name && name1.score == name2.score) gleiche++;
        }
    }
    return gleiche;
}
