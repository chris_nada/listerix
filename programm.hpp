#pragma once

#include <FL/Fl.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Box.H>
#include <map>
#include <vector>

class Programm final {

    struct Name {
        std::string name;
        int score   = 0;
        int gesehen = 0;
        std::map<std::string, bool> besser_als;
    };

public:

    Programm();

    int start();

private:

    static void auswahl(Fl_Widget* w, void* data);

    bool liste_lesen();

    void set_links(Name& name);

    void set_rechts(Name& name);

    void next();

    size_t anzahl_gleich();

    /// Bei Nichtauffinden: return liste.size();
    size_t pos(const std::string& eintrag) const;

    std::string slinks;

    std::string srechts;

    std::string stodo;

    std::vector<Name> liste;

    Fl_Window* fenster;

    Fl_Box* box_text;

    Fl_Box* box_todo;

    Fl_Button* btn_links;

    Fl_Button* btn_rechts;
};