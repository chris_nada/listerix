cmake_minimum_required(VERSION 3.7)
project(listerix)

set(EXECUTABLE_OUTPUT_PATH ${CMAKE_SOURCE_DIR}/listerix)
set(CMAKE_CXX_STANDARD 17)
if (CMAKE_BUILD_TYPE STREQUAL "Release")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Ofast -s -mtune=generic")
else()
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra -Werror -g")
endif()

find_package(FLTK)
include_directories(listerix SYSTEM
        ${FLTK_INCLUDE_DIR}
        )

add_executable(listerix
        main.cpp
        programm.cpp programm.hpp)

target_link_libraries(listerix ${FLTK_LIBRARIES})
